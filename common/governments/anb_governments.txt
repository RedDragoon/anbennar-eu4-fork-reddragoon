

orcish_horde = {
	monarchy = yes

	unique_government = yes

	color = { 210 195 35 }
	
	tribal = yes
	nomad = yes	
	allow_migration = yes
	maintain_dynasty = yes
	
	valid_for_new_country = no
	allow_convert = no
	valid_for_nation_designer = yes
	nation_designer_cost = 0	
	nation_designer_trigger = {
		technology_group = tech_orcish
	}
	ai_will_do = {
		factor = 0
	}

	rank = {
		1 = {
			global_manpower_modifier = 0.1
			land_forcelimit_modifier = 0.1
			loot_amount = 0.50
			global_institution_spread = -0.15
			reinforce_cost_modifier = -0.5
			movement_speed = 0.2
			technology_cost = -0.1
			diplomatic_upkeep = -2
		}
		2 = {
			global_manpower_modifier = 0.2
			land_forcelimit_modifier = 0.2	
			loot_amount = 0.50
			global_institution_spread = -0.15
			reinforce_cost_modifier = -0.5
			movement_speed = 0.2
			diplomatic_upkeep = -2
		}
		3 = {
			global_manpower_modifier = 0.3
			land_forcelimit_modifier = 0.3
			loot_amount = 0.50
			global_institution_spread = -0.15
			reinforce_cost_modifier = -0.5
			movement_speed = 0.2
			diplomatic_upkeep = -2
		}
	}
}

goblin_horde = {
	monarchy = yes

	unique_government = yes

	color = { 210 195 35 }
	
	tribal = yes
	nomad = yes	
	allow_migration = yes
	
	valid_for_new_country = no
	allow_convert = no
	valid_for_nation_designer = yes
	nation_designer_cost = 0	
	nation_designer_trigger = {
		technology_group = tech_goblin
	}
	ai_will_do = {
		factor = 0
	}

	rank = {
		1 = {
			global_manpower_modifier = 0.1
			land_forcelimit_modifier = 0.1
			loot_amount = 0.50
			global_institution_spread = -0.15
			reinforce_cost_modifier = -0.5
			infantry_cost = -0.10
			movement_speed = 0.2
		}
		2 = {
			global_manpower_modifier = 0.2
			land_forcelimit_modifier = 0.2	
			loot_amount = 0.50
			global_institution_spread = -0.15
			reinforce_cost_modifier = -0.5
			infantry_cost = -0.10
			movement_speed = 0.2
		}
		3 = {
			global_manpower_modifier = 0.3
			land_forcelimit_modifier = 0.3
			loot_amount = 0.50
			global_institution_spread = -0.15
			reinforce_cost_modifier = -0.5
			infantry_cost = -0.10
			movement_speed = 0.2
		}
	}
}

elven_principality = {	#Contrary to the name, this is actually a republic - after all, elves live for ages!
	republic = yes
	
	unique_government = yes
	
	valid_for_new_country = yes
	valid_for_nation_designer = yes
	nation_designer_cost = 0	
	
	color = { 174	243  240 }
	
	duration = 20 #20 years because thats the length of a generation of humans to grow to adulthood

	republican_name = no
	royal_marriage = no
	
	ai_will_do = {
		factor = 0
	}
	ai_importance = 1

	#bonus
	max_states = 2
	
	rank = {
		1 = {
			global_unrest = -1
			production_efficiency = 0.15
			stability_cost_modifier = -0.05
			diplomatic_reputation = 1
			max_absolutism = -50
		}
		2 = {
			global_unrest = -1
			production_efficiency = 0.2
			stability_cost_modifier = -0.1
			diplomatic_reputation = 1
			max_absolutism = -50
		}
		3 = {
			global_unrest = -1
			production_efficiency = 0.2
			stability_cost_modifier = -0.15
			diplomatic_reputation = 1
			max_absolutism = -50
		}
	}
}

magisterium = {
	religion = yes
	#papacy = yes
	
	unique_government = yes
	
	allow_force_tributary = no
	
	color = { 230 195 195 }
	
	valid_for_new_country = no
	allow_convert = no

	royal_marriage = no
	
	rulers_can_be_generals = yes
	has_devotion = yes
	
	max_states = 1
	
	ai_will_do = {
		factor = 0
	}

	rank = {
		2 = {
			tolerance_own = 1
			prestige = 1
			diplomatic_reputation = 1
		}
	}
	fixed_rank = 2
	
	#faction = magi_noble_mages
	#faction = magi_common_mages
}

knightly_order = {
	religion = yes	#Makes em a theocracy with theocracy succession choosing events
	monastic = yes	#Monastic orders can only be duchy rank
	allow_migration = yes
	
	color = { 230 155 152 }
	
	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 10
	
	royal_marriage = no
	
	#They're secular
	#different_religion_acceptance = -20
	#different_religion_group_acceptance = -50
	
	ai_will_do = {
		factor = 0
	}
	
	has_devotion = yes #Will need to sort out the religious devotion events for these dudes though
	
	max_states = 1
	
	rank = {
		1 = {
			global_manpower_modifier = 0.25
			fort_maintenance_modifier = -0.2
		}
	}
	fixed_rank = 1	
}

adventurer = {	#For now a theocracy is the closest thing
	#native_mechanic = yes
	religion = yes	#Makes em a theocracy with theocracy succession choosing events
	#monastic = yes	#Monastic orders can only be duchy rank
	allow_migration = yes
	
	color = { 167 111 152 }
	
	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 10
	
	republican_name = yes
	royal_marriage = no
	
	militarised_society = yes
	
	#They're secular
	#different_religion_acceptance = -20
	#different_religion_group_acceptance = -50
	
	faction = adv_marchers
	faction = adv_pioneers
	faction = adv_fortune_seekers
	
	ai_will_do = {
		factor = 0
	}
	
	has_devotion = yes #Will need to sort out the religious devotion events for these dudes though
	
	max_states = 1
	
	rank = {
		1 = {
			manpower_recovery_speed = 0.2
			colonists = 1
			migration_cooldown = -0.5
			diplomats = -1
			diplomatic_upkeep = -2
		}
		2 = {
			manpower_recovery_speed = 0.2
			colonists = 1
			migration_cooldown = -0.5
			diplomats = -1
			diplomatic_upkeep = -2
		}
		3 = {
			manpower_recovery_speed = 0.2
			colonists = 1
			migration_cooldown = -0.5
			diplomats = -1
			diplomatic_upkeep = -2
		}
	}
	#fixed_rank = 1	
}