country_decisions = {

	castellyr_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_castellyr }
			NOT = { exists = B31 } #Castellyr doesn't exist
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			culture_group = escanni
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		allow = {
			#adm_tech = 10
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			#num_of_cities = 5
			owns_core_province = 840	#North Citadel
			owns_core_province = 833	#North Castonath
			owns_core_province = 831	#South Castonath
			owns_core_province = 832	#West Castonath
			num_of_owned_provinces_with = {
				value = 10
				region = inner_castanor_region
			}
		}
		effect = {
			change_tag = B31
			remove_non_electors_emperors_from_empire_effect = yes
			if = {
				limit = {
					NOT = { government_rank = 2 }
				}
				set_government_rank = 2
			}
			
			#Claims
			castonath_area = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_claim = B31
			}
			trialmount_area = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_claim = B31
			}
			lower_nath_area = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_claim = B31
			}
			upper_nath_area = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_claim = B31
			}
			westgate_area = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_claim = B31
			}
			southgate_area = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_claim = B31
			}
			area68_area = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_claim = B31
			}
			
			
			add_prestige = 50
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = formed_castellyr_flag
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	castanor_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_castanor }
			NOT = { exists = B32 } #Castanor doesn't exist
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			culture_group = escanni
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		allow = {
			#adm_tech = 10
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			#num_of_cities = 5
			
			#Requires provinces
			owns_core_province = 840	#North Citadel
			owns_core_province = 833	#North Castonath
			owns_core_province = 831	#South Castonath
			owns_core_province = 832	#West Castonath
			owns_core_province = 754	#Westgate
			owns_core_province = 827
			
			owns_core_province = 823	#Burnoll
			owns_core_province = 534	#South Citadel
			owns_core_province = 229	#Balmire
			
			inner_castanor_region = {
				type = all
				owned_by = ROOT
			}
		}
		effect = {
			change_tag = B32
			remove_non_electors_emperors_from_empire_effect = yes
			if = {
				limit = {
					NOT = { government_rank = 3 }
				}
				set_government_rank = 3
			}
			840 = {	#To North Citadel
				move_capital_effect = yes
			}
			hidden_effect = {
				every_owned_province = {
					limit = {
						has_owner_culture = yes
					}
					change_culture = castanorian
				}
			}
			change_primary_culture = castanorian
			
			#Claims
			inner_castanor_region = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = B32
			}
			west_castanor_region = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = B32
			}
			inner_castanor_region = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = B32
			}
			
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			
			add_prestige = 50
			add_country_modifier = {
				name = "centralization_modifier"
				duration = 7300
			}
			set_country_flag = formed_castanor_flag
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}

	blademarches_nation = {
		major = yes
		potential = {
			capital_scope = {
				OR = {
					region = west_castanor_region
					region = south_castanor_region
					region = inner_castanor_region
				}
			}
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_blademarches }
			NOT = { exists = B33 } #Blademarches doesn't exist
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			#culture_group = escanni
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		allow = {
			#adm_tech = 10
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			#num_of_cities = 5
			
			#Ruler Prerequisites
			OR = {
				mil = 5 #Ruler has at least 5 mil
				ruler_has_personality = bold_fighter_personality
				ruler_has_personality = inspiring_leader_personality
				ruler_has_personality = strict_personality
				ruler_has_personality = tactical_genius_personality
				ruler_has_personality = conqueror_personality
			}
			NOT = { ruler_has_personality = craven_personality }
			
			#Province Prerequisites
			num_of_owned_provinces_with = {
				value = 5
				OR = {
					area = area61_area
					area = area60_area
					area = area59_area
					area = area56_area
					area = area55_area
					area = dostans_way_area
					area = area58_area
				}
			}
		}
		effect = {
			change_tag = B33
			remove_non_electors_emperors_from_empire_effect = yes
			if = {
				limit = {
					NOT = { government_rank = 2 }
				}
				set_government_rank = 2
			}
			
			#Claims
			area61_area = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_claim = B33
			}
			area60_area = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_claim = B33
			}
			area59_area = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_claim = B33
			}
			area56_area = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_claim = B33
			}
			area55_area = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_claim = B33
			}
			dostans_way_area = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_claim = B33
			}
			area58_area = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_claim = B33
			}
			
			
			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = formed_blademarches_flag
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
}