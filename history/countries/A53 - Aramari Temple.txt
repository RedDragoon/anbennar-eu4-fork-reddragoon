government = theocratic_government
government_rank = 1
primary_culture = crownsman
religion = regent_court
technology_group = tech_cannorian
capital = 249


1437.3.1 = {
	monarch = {
		name = "Marion the Transmuter"
		birth_date = 1376.9.3
		adm = 6
		dip = 0
		mil = 0
	}
}