government = merchant_republic
government_rank = 1
primary_culture = nathalairey
religion = regent_court
technology_group = tech_cannorian
national_focus = DIP
capital = 451

1440.1.1 = {
	monarch = {
		name = "Shadow Council"
		adm = 3
		dip = 3
		mil = 2
		regent = yes
	}
}