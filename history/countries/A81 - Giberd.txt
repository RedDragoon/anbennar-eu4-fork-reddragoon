government = imperial_city
government_rank = 1
mercantilism = 25
primary_culture = imperial_gnome
religion = regent_court
technology_group = tech_gnomish
capital = 271 #Giberd
national_focus = DIP

1399.1.1 = {
	monarch = {
		name = "City Council"
		adm = 2
		dip = 2
		mil = 2
		regent = yes
	}
}