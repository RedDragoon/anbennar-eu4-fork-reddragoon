#107 - Brescia | West Ording

owner = A02
controller = A02
add_core = A02
add_core = A17

culture = low_lorentish
religion = regent_court

hre = no

base_tax = 3
base_production = 4   
base_manpower = 2   
 
trade_goods = livestock

capital = "West Ording" 
is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish

