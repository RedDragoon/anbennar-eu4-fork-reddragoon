# No previous file for Tandil
culture = green_orc
religion = great_dookan
capital = ""

hre = no

base_tax = 2
base_production = 3
base_manpower = 2

trade_goods = iron

native_size = 78
native_ferocity = 9
native_hostileness = 9

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_orcish
discovered_by = tech_goblin
discovered_by = tech_bulwari