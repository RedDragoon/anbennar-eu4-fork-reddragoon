# No previous file for Potosi
owner = B23
controller = B23
add_core = B23
culture = green_orc
religion = great_dookan
capital = ""

hre = no

base_tax = 2
base_production = 1
base_manpower = 2

trade_goods = cloth

native_size = 10
native_ferocity = 7
native_hostileness = 5